FROM registry.globogis.it/stu/validator-devel/validator-devel:latest

ARG GITLAB_ACCESS_TOKEN
RUN wget --header="PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" -O/tmp/validator.zip https://git.globogis.it/api/v4/projects/13/repository/archive.zip?sha=1.0-dev && \
    unzip /tmp/validator.zip -d /tmp/validator && \
    mkdir /validator && \
    mv /tmp/validator/*/* /validator && \
    rm -r /tmp/validator && \
    rm /tmp/validator.zip
COPY *.html /modulistica/
COPY modules /modulistica/modules/
